// Import Vue
import Vue from 'vue';
// We use axios HTTP client. See https://github.com/axios/axios for docs.
import axios from 'axios';

// Promise support for IE browser, see https://vuex.vuejs.org/installation.html
import 'es6-promise/auto';
// Import vue-i18n for internationalization
import VueI18n from 'vue-i18n'

// Import Vuelidate library for validation input
import Vuelidate from 'vuelidate';

// Import F7
import Framework7 from 'framework7/framework7.esm.bundle.js';

// Import F7 Vue Plugin
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle.js';

// Import F7 Styles
import 'framework7/css/framework7.bundle.css';

// Import Icons and App Custom Styles
import IconsStyles from './scss/icons.scss';
import AppStyles from './scss/app.scss';

// Import App Component
import App from './app.vue';

import { Language, Env, ApiUrl } from './services/misc/constants';

// Init VueI18n
Vue.use(VueI18n);

// Init Vuelidate library
Vue.use(Vuelidate);

// Init F7 Vue Plugin
Framework7.use(Framework7Vue);

// set defaults internationalization as initial
// latest translation files will be retrieved from server
// in refreshAppTranslations action
const i18n = new VueI18n({
  locale: Language.GERMAN,
  messages: {
    [Language.ENGLISH]: require('../static/locale/en'),
    [Language.GERMAN]: require('../static/locale/de'),
  },
});

// Init Vuex Store and import store file
import { store } from './store';

// Import and install offline-plugin
import * as OfflinePluginRuntime from 'offline-plugin/runtime';
OfflinePluginRuntime.install();

// Init App
new Vue({
  el: '#app',
  template: '<app/>',
  i18n,
  // Register Vuex store to Vue instance
  store,
  // Register App Component
  components: {
    app: App
  },
  created() {
    window.addEventListener('online',  this.onlineIndicator);
    window.addEventListener('offline', this.onlineIndicator);
    this.$store.commit('core/setAppDevice', Framework7.device);
    this.$store.commit('core/setAwsBucket', aws.bucket);
    this.$store.commit('core/setAppVersion', app.version);
    if (Framework7.device.android) {
      document.write('<meta name="viewport" content="width=device-width,height='+window.innerHeight+', initial-scale=1.0">');
    }

    // set default url
    const apiUrl = process.env === Env.PRODUCTION ? ApiUrl.PRODUCTION : ApiUrl.DEVELOPMENT;
    this.$store.commit('core/setApiUrl', apiUrl);
    // interceptor of 401 errors to refresh the session key.
    this.createAxiosResponseInterceptor();
    // check authentication status
    this.$store.dispatch('core/getAuthenticationStatus');
    // synchronize translations
    this.$store.dispatch('core/refreshAppTranslations', i18n);
  },
  methods: {
    onlineIndicator() {
      if (navigator.onLine) {
        this.$store.commit('core/setIsOfflineState', false);
        this.$store.dispatch('eventsItem/checkSubmissionsInLocalForage').then((value) => {
          if (value) {
            this.$store.dispatch('eventsItem/sendOfflineSubmissions');
          }
        });
      } else {
        this.$store.commit('core/setIsOfflineState', true);
      }
    },
    createAxiosResponseInterceptor() {
      const interceptor = axios.interceptors.response.use(response => response,
        error => {
          if (error.response.status !== 401) {
            return Promise.reject(error.response.data);
          }
          axios.interceptors.response.eject(interceptor);
          return this.$store.dispatch('core/refreshSessionKey', this.$store.getters['core/refreshToken'])
            .then(()=> {
              error.response.config.headers['Authorization'] = `${this.$store.getters['core/tokenType']} ${this.$store.getters['core/sessionKey']}`;
              return axios(error.response.config);
            })
            .catch(error => {
              this.$store.dispatch('core/logout').then(() => {
                this.$f7.view.main.history.pop();
                this.$f7.views.main.router.navigate('/login');
                location.reload();
              });
              return Promise.reject(error);
            })
            .finally(this.createAxiosResponseInterceptor);
        }
      );
    }
  }
});
