export default {
  userAuthenticated: (state) => {
    return state.authenticated;
  },

  isHashUserUpdated: (state) => {
    return state.hashUserUpdated;
  },

  userInformation: (state) => {
    return state.user;
  },

  apiUrl: (state) => {
    return state.apiRoot;
  },

  sessionKey: (state) => {
    return state.sessionKey;
  },

  refreshToken: (state) => {
    return state.refreshToken;
  },

  tokenType: (state) => {
    return state.tokenType;
  },

  appDevice: (state) => {
    return state.appDevice;
  },

  temporaryCredentials: (state) => {
    return state.temporaryCredentials;
  },

  awsBucket: (state) => {
    return state.awsBucket;
  },

  user: (state) => {
    return state.user;
  },

  appVersion: (state) => {
    return state.appVersion;
  },

  isOffline: (state) => {
    return state.isOffline;
  }
}
