import * as Cookies from 'js-cookie';
import * as AWS from 'aws-sdk';
import axios from 'axios';
import localForage from 'localforage';
import {
  SessionCookieName,
  RefreshTokenCookieName,
  TokenTypeCookieName,
  TemporaryCredentialsCookieName
} from '../../services/misc/constants';
import { sendRegistrationData, checkInvitationToken } from '../../services/registration';
import { fetchSessionKey, fetchRefreshedSessionKey } from '../../services/login';
import { fetchUser } from '../../services/user';
import { fetchTranslations } from '../../services/misc';

export default {
  getAuthenticationStatus({ state, dispatch, commit }) {
    const sessionKey = Cookies.get(SessionCookieName);
    const refreshToken = Cookies.get(RefreshTokenCookieName);
    const tokenType = Cookies.get(TokenTypeCookieName);
    let temporaryCredentials = Cookies.get(TemporaryCredentialsCookieName);
    const authenticationStatus = Boolean(sessionKey);
    if (authenticationStatus) {
      temporaryCredentials = JSON.parse(temporaryCredentials);
      commit('setSessionKey', sessionKey);
      commit('setRefreshToken', refreshToken);
      commit('setTokenType', tokenType);
      commit('setTemporaryCredentials', temporaryCredentials);
      AWS.config.update({
        accessKeyId: temporaryCredentials.awsAccessKey, secretAccessKey: temporaryCredentials.awsSecretKey,
        sessionToken: temporaryCredentials.sessionToken,
        region: 'eu-central-1'
      });
      axios.defaults.headers.common['Authorization'] = `${ state.tokenType } ${ state.sessionKey }`;
      dispatch('requestUser');
    }
    dispatch('updateAuthenticationStatus', authenticationStatus);
  },
  updateAuthenticationStatus({ commit }, status) {
    commit('authenticationStatusUpdated', status);
  },

  storeSessionKey(context, sessionKey) {
    context.commit('setSessionKey', sessionKey);
    Cookies.set(SessionCookieName, sessionKey);
  },

  removeSessionKey() {
    Cookies.remove(SessionCookieName);
  },

  storeRefreshToken(context, refreshToken) {
    context.commit('setRefreshToken', refreshToken);
    Cookies.set(RefreshTokenCookieName, refreshToken);
  },

  removeRefreshToken(context) {
    context.commit('setRefreshToken', '');
    Cookies.remove(RefreshTokenCookieName);
  },

  storeTokenType(context, refreshToken) {
    context.commit('setTokenType', refreshToken);
    Cookies.set(TokenTypeCookieName, refreshToken);
  },

  removeTokenType(context) {
    context.commit('setTokenType', '');
    Cookies.remove(TokenTypeCookieName);
  },

  storeTemporaryCredentials(context, temporaryCredentials) {
    context.commit('setTemporaryCredentials', temporaryCredentials);
    Cookies.set(TemporaryCredentialsCookieName, temporaryCredentials);
  },

  removeTemporaryCredentials(context) {
    context.commit('setTemporaryCredentials', '');
    Cookies.remove(TemporaryCredentialsCookieName);
  },

  requestUser({ state, commit, rootState }) {
    fetchUser().then((user) => {
      localForage.getItem('user').then((userInStore) => {
        if (!userInStore) {
          commit('setHashUserUpdated', true);
          commit('setUser', { user });
          localForage.setItem('user', { user }).catch(error => {
            console.log(error.message);
          });
        } else if (userInStore.user.templateStructureHash === user.templateStructureHash) {
          commit('setHashUserUpdated', false);
          commit('setUser', { user });
        } else {
          commit('setHashUserUpdated', true);
          commit('setUser', { user });
          localForage.setItem('user', { user }).catch(error => {
            console.log(error.message);
          });
        }
      });
    });
  },

  requestCheckInvitationToken({}, token) {
    return new Promise((resolve, reject) => {
      resolve();
      checkInvitationToken(token).then(() => {
        resolve();
      }).catch((error) => {
        reject(error);
      });
    });
  },

  completeRegistration(context, data) {
    return new Promise((resolve, reject) => {
      sendRegistrationData(data).then((sessionKey) => {
        const authenticationStatus = true;
        context.dispatch('updateAuthenticationStatus', authenticationStatus);
        context.dispatch('storeSessionKey', sessionKey.accessToken);
        context.dispatch('storeRefreshToken', sessionKey.refreshToken);
        context.dispatch('storeTokenType', sessionKey.tokenType);
        context.dispatch('storeTemporaryCredentials', sessionKey.temporaryCredentials);
        AWS.config.update({
          accessKeyId: sessionKey.temporaryCredentials.awsAccessKey, secretAccessKey: sessionKey.temporaryCredentials.awsSecretKey,
          sessionToken: sessionKey.temporaryCredentials.sessionToken,
          region: 'eu-central-1'
        });
        axios.defaults.headers.common['Authorization'] = `${sessionKey.tokenType} ${sessionKey.accessToken}`;
        resolve();
      }).catch(function (error) {
        console.log(error);
        reject(error);
      });
    });
  },

  login(context, credentials) {
    return new Promise((resolve, reject) => {
      fetchSessionKey(credentials).then((sessionKey) => {
        const authenticationStatus = true;
        context.dispatch('updateAuthenticationStatus', authenticationStatus);
        context.dispatch('storeSessionKey', sessionKey.accessToken);
        context.dispatch('storeRefreshToken', sessionKey.refreshToken);
        context.dispatch('storeTokenType', sessionKey.tokenType);
        context.dispatch('storeTemporaryCredentials', sessionKey.temporaryCredentials);
        AWS.config.update({
          accessKeyId: sessionKey.temporaryCredentials.awsAccessKey,
          secretAccessKey: sessionKey.temporaryCredentials.awsSecretKey,
          sessionToken: sessionKey.temporaryCredentials.sessionToken,
          region: 'eu-central-1'
        });
        axios.defaults.headers.common['Authorization'] = `${ sessionKey.tokenType } ${ sessionKey.accessToken }`;
        resolve(true);
      }).catch(function (error) {
        reject(error);
      });
    });
  },

  logout(context) {
    return new Promise((resolve) => {
      const authenticationStatus = false;
      context.dispatch('updateAuthenticationStatus', authenticationStatus);
      context.dispatch('removeSessionKey');
      context.dispatch('removeTokenType');
      context.dispatch('removeRefreshToken');
      context.dispatch('removeTemporaryCredentials');
      context.commit('setUser', { user: {} });

      localForage.clear().then(resolve).catch(resolve);
    });
  },

  refreshSessionKey({ state, dispatch }, refreshToken) {
    return new Promise((resolve, reject) => {
      fetchRefreshedSessionKey(refreshToken)
        .then((newValues) => {
          const authenticationStatus = true;
          dispatch('updateAuthenticationStatus', authenticationStatus);
          dispatch('storeSessionKey', newValues.accessToken);
          dispatch('storeRefreshToken', newValues.refreshToken);
          dispatch('storeTokenType', newValues.tokenType);
          dispatch('storeTemporaryCredentials', newValues.temporaryCredentials);
          AWS.config.update({
            accessKeyId: newValues.temporaryCredentials.awsAccessKey,
            secretAccessKey: newValues.temporaryCredentials.awsSecretKey,
            sessionToken: newValues.temporaryCredentials.sessionToken,
            region: 'eu-central-1'
          });
          axios.defaults.headers.common['Authorization'] = `${ newValues.tokenType } ${ newValues.accessToken }`;
          resolve(newValues);
        })
        .catch((error) => {
          dispatch('removeSessionKey');
          dispatch('removeTokenType');
          dispatch('removeRefreshToken');
          console.log(error);
          reject(error);
        });
    });
  },

  refreshAppTranslations({ state }, i18n) {
    const lang = state.lang;
    fetchTranslations(lang).then((messages) => {
      i18n.setLocaleMessage(lang, messages);
    });
  },

};
