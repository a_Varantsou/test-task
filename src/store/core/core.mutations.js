export default {
  setSessionKey(state, payload) {
    state.sessionKey = payload;
  },

  setRefreshToken(state, payload) {
    state.refreshToken = payload;
  },

  setTokenType(state, payload) {
    state.tokenType = payload;
  },

  setUser(state, payload) {
    state.user = payload.user;
  },

  authenticationStatusUpdated(state, status) {
    state.authenticated = status;
  },

  setHashUserUpdated(state, payload) {
    state.hashUserUpdated = payload;
  },

  setApiUrl(state, payload) {
    state.apiRoot = payload;
  },

  setAppDevice(state, payload) {
    state.appDevice = payload;
  },

  setAwsBucket(state, payload) {
    state.awsBucket = payload;
  },

  setAppVersion(state, payload) {
    state.appVersion = payload;
  },

  setIsOfflineState(state, payload) {
    state.isOffline = payload;
  }
}
