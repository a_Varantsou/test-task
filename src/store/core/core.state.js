import { Language } from '../../services/misc/constants';

export default {
  authenticated: false,
  user: {},
  apiRoot: '',
  hashUserUpdated: false,
  lang: Language.ENGLISH,
  sessionKey: null,
  refreshToken: null,
  tokenType: null,
  temporaryCredentials: {
    awsAccessKey: '',
    awsSecretKey: '',
    sessionToken: ''
  },
  awsBucket: '',
  appDevice: {},
  isOffline: false,
};
