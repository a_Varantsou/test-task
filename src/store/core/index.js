import state from './core.state';
import actions from './core.actions';
import getters from './core.getters';
import mutations from './core.mutations';

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
};
