// Import Vue and Vuex
import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

Vue.use(Vuex);

// Modules
import core from './core';

export const store = new Vuex.Store({
  modules: {
    core
  },
  strict: false,
  plugins: [ createLogger() ],
});
