export const Language = {
  ENGLISH: 'en',
  GERMAN: 'de',
};

export const SessionCookieName = 'sk';
export const RefreshTokenCookieName = 'rt';
export const TokenTypeCookieName = 'tt';
export const TemporaryCredentialsCookieName = 'tc';

export const Env = {
  PRODUCTION: 'production',
  DEVELOPMENT: 'development'
};

export const ErrorName = {
  LOGIN_FAILED: 'login_failed'
};

export const ApiUrl = {
  PRODUCTION: 'https://app.protime-event.com:8081',
  DEVELOPMENT: 'https://piqayo.com:8081'
};
