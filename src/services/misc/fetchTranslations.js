import axios from 'axios';

export const fetchTranslations = (lang) => {
  const url = `/static/locale/${ lang }.json`;

  return axios.get(url)
    .then((response) => {
      const translations = response.data;
      return Promise.resolve(translations);
    })
    .catch(error => Promise.reject(error));
};
