import axios from 'axios';
import { store } from '../../store';

export const fetchSessionKey = credentials => {
  const mockUrl = `${ store.getters['core/apiUrl'] }/auth/login`;

  const params = {
    email: credentials.mailAddress,
    password: credentials.password
  };

  return axios.post(mockUrl, params)
    .then((response) => {
      return Promise.resolve(response.data);
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};
