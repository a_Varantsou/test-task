import axios from 'axios';
import { store } from '../../store';

export const fetchRefreshedSessionKey = refreshToken => {
  const mockUrl = `${ store.getters['core/apiUrl'] }/auth/refresh-token`;

  const params = {
    params: {
      refreshToken
    }
  };

  return axios.post(mockUrl, {}, params)
    .then((response) => {
      return Promise.resolve(response.data);
    })
    .catch((error) => {
      return Promise.reject(error.response);
    });
};
