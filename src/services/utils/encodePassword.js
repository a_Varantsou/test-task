export const encodePassword = password => {
  return password.split('').reduce((hash, symbol) =>
    (((hash << 5) - hash) + symbol.charCodeAt(0))|0, 0);
};
