import axios from 'axios';
import { store } from '../../store';

export const checkInvitationToken = (token) => {
  const mockUrl = `${ store.getters['core/apiUrl'] }/invitation/check-token`;

  const request = {
    invitationToken: token
  };

  return axios.post(mockUrl, request)
    .then(({ data }) => {
      return Promise.resolve(data);
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};
