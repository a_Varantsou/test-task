import axios from 'axios';
import { store } from '../../store';

export const sendRegistrationData = data => {
  const mockUrl = `${ store.getters['core/apiUrl'] }/invitation/accept`;

  return axios.post(mockUrl, data)
    .then(({ data }) => {
      return Promise.resolve(data);
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

