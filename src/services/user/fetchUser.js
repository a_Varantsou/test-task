import axios from 'axios';
import { store } from '../../store';

export const fetchUser = () => {
  const mockUrl = `${ store.getters['core/apiUrl'] }/driver/driver-file/personal-data`;

  return axios.get(mockUrl)
    .then(({ data }) => Promise.resolve(data))
    .catch(error => Promise.reject(error));
};
