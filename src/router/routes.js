import CoursePage from '../pages/course.vue';
import NotFoundPage from '../pages/not-found.vue';

import PanelLeftPage from '../pages/panel-left.vue';

import { authenticatedAccessGuard, guestAccessGuard } from './routes-guards';

export const routes = [
  {
    name: 'home',
    path: '/',
    async(routeTo, routeFrom, resolve, reject) {
      authenticatedAccessGuard('home', routeTo, resolve, reject);
    },
    options: {
      auth: true,
    },
  },
  {
    name: 'registration',
    path: '/registration',
    async(routeTo, routeFrom, resolve, reject) {
      guestAccessGuard('registration', routeTo, resolve, reject)
    },
    options: {
      auth: false,
    },
  },
  {
    path: '/panel-left/',
    component: PanelLeftPage,
  },
  {
    name: 'course',
    path: '/course',
    async(routeTo, routeFrom, resolve, reject) {
      authenticatedAccessGuard('course', routeTo, resolve, reject)
    },
    options: {
      auth: true,
    },
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];
