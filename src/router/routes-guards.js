import { store } from '../store';

export const authenticatedAccessGuard = (pageName, routeTo, resolve, reject) => {
  const isAuthenticated = routeTo.route.options.auth && store.getters['core/userAuthenticated'];
  import(`../pages/${ isAuthenticated ? pageName : 'login' }`)
    .then(vc => resolve({ component: vc.default }))
    .catch(reject);
};

export const guestAccessGuard = (pageName, routeTo, resolve, reject) => {
  const isAuthenticated = store.getters['core/userAuthenticated'];
  if (routeTo.name === 'registration' && !isAuthenticated) {
    store.dispatch('core/requestCheckInvitationToken', routeTo.query.token)
      .then(() => {
        import(`../pages/${ pageName }`)
          .then(vc => resolve({ component: vc.default }))
          .catch(reject);
      })
      .catch(() => {
        import(`../pages/login`)
          .then(vc => resolve({ component: vc.default }, { reloadAll: true }))
          .catch(reject);
      });
  }
};
